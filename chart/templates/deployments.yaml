apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "trackableappname" . }}
  annotations:
    {{ if .Values.gitlab.app }}app.gitlab.com/app: {{ .Values.gitlab.app | quote }}{{ end }}
    {{ if .Values.gitlab.env }}app.gitlab.com/env: {{ .Values.gitlab.env | quote }}{{ end }}
  labels:
    app: corpora
spec:
  replicas: {{ .Values.replicaCount }}
{{- if .Values.strategyType }}
  strategy:
    type: {{ .Values.strategyType | quote }}
{{- end }}
  selector:
    matchLabels:
      app: corpora
      component: platform
  template:
    metadata:
      annotations:
        checksum/application-secrets: "{{ .Values.application.secretChecksum }}"
        {{ if .Values.gitlab.app }}app.gitlab.com/app: {{ .Values.gitlab.app | quote }}{{ end }}
        {{ if .Values.gitlab.env }}app.gitlab.com/env: {{ .Values.gitlab.env | quote }}{{ end }}
      labels:
        app: corpora
        component: platform
    spec:
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - corpora
              - key: component
                operator: In
                values:
                - cantaloupe
            topologyKey: "kubernetes.io/hostname"
      imagePullSecrets:
{{ toYaml .Values.image.secrets | indent 10 }}
      containers:
      - name: platform
        image: {{ template "imagename" . }}
        imagePullPolicy: {{ .Values.image.pullPolicy }}
{{- if .Values.application.secretName }}
        envFrom:
        - secretRef:
            name: {{ .Values.application.secretName }}
{{- end }}
        ports:
        - containerPort: 8000
        - containerPort: 9999
        env:
        - name: GITLAB_ENVIRONMENT_NAME
          value: {{ .Values.gitlab.envName }}
        - name: GITLAB_ENVIRONMENT_URL
          value: {{ .Values.gitlab.envURL }}
        resources:
          requests:
            cpu: 100m
            memory: 500Mi
        livenessProbe:
          httpGet:
            path: "/scholar"
            scheme: "HTTP"
            port: 8000
          initialDelaySeconds: 25
          timeoutSeconds: 5
          periodSeconds: 30
        readinessProbe:
          httpGet:
            path: "/scholar"
            scheme: "HTTP"
            port: 8000
          initialDelaySeconds: 25
          timeoutSeconds: 5
          periodSeconds: 30
        volumeMounts:
        - name: corpora-data
          mountPath: /static
          subPath: static
        - name: corpora-data
          mountPath: /corpora
          subPath: corpora
        - name: corpora-data
          mountPath: /conf
          subPath: conf
        - name: configmap-volume
          mountPath: /etc/ImageMagick-6/policy.xml
          subPath: policy.xml
{{- if .Values.initContainers }}
      initContainers:
{{ toYaml .Values.initContainers | indent 8 }}
{{- end }}
      volumes:
      - name: corpora-data
        persistentVolumeClaim:
          claimName: corpora-pvc
      - name: configmap-volume
        configMap:
          name: imagemagick-xml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  annotations:
    {{ if .Values.gitlab.app }}app.gitlab.com/app: {{ .Values.gitlab.app | quote }}{{ end }}
    {{ if .Values.gitlab.env }}app.gitlab.com/env: {{ .Values.gitlab.env | quote }}{{ end }}
  labels:
    app: corpora
spec:
  selector:
    matchLabels:
      app: corpora
      component: nginx
  template:
    metadata:
      annotations:
        checksum/application-secrets: "{{ .Values.application.secretChecksum }}"
        {{ if .Values.gitlab.app }}app.gitlab.com/app: {{ .Values.gitlab.app | quote }}{{ end }}
        {{ if .Values.gitlab.env }}app.gitlab.com/env: {{ .Values.gitlab.env | quote }}{{ end }}
      labels:
        app: corpora
        component: nginx
    spec:
      affinity:
        podAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - corpora
                - key: component
                  operator: In
                  values:
                  - cantaloupe
              topologyKey: "kubernetes.io/hostname"
      imagePullSecrets:
{{ toYaml .Values.image.secrets | indent 10 }}
      containers:
      - name: nginx
        image: nginx:1.23
        imagePullPolicy: {{ .Values.image.pullPolicy }}
{{- if .Values.application.secretName }}
        envFrom:
        - secretRef:
            name: {{ .Values.application.secretName }}
{{- end }}
        ports:
        - containerPort: 80
        resources:
          requests:
            cpu: 100m
            memory: 500Mi
        volumeMounts:
        - name: corpora-data
          mountPath: /data/corpora/static
          subPath: static
        - name: corpora-data
          mountPath: /data/corpora/corpora
          subPath: corpora
        - name: configmap-volume
          mountPath: /etc/nginx/conf.d/default.conf
          subPath: default.conf
      initContainers:
      - name: wait-for-it
        image: busybox:1.28.4
        # wait for Corpora pod
        command: ["/bin/sh", "-c"]
        args:
          - echo starting;
            until nslookup corpora; do echo waiting for Corpora; sleep 3; done;
            echo done waiting;
      volumes:
      - name: corpora-data
        persistentVolumeClaim:
          claimName: corpora-pvc
      - name: configmap-volume
        configMap:
          name: nginx-conf
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cantaloupe
  annotations:
    {{ if .Values.gitlab.app }}app.gitlab.com/app: {{ .Values.gitlab.app | quote }}{{ end }}
    {{ if .Values.gitlab.env }}app.gitlab.com/env: {{ .Values.gitlab.env | quote }}{{ end }}
  labels:
    app: corpora
spec:
  selector:
    matchLabels:
      app: corpora
      component: cantaloupe
  template:
    metadata:
      annotations:
        checksum/application-secrets: "{{ .Values.application.secretChecksum }}"
        {{ if .Values.gitlab.app }}app.gitlab.com/app: {{ .Values.gitlab.app | quote }}{{ end }}
        {{ if .Values.gitlab.env }}app.gitlab.com/env: {{ .Values.gitlab.env | quote }}{{ end }}
      labels:
        app: corpora
        component: cantaloupe
    spec:
      affinity:
        podAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - corpora
                - key: component
                  operator: In
                  values:
                  - cantaloupe
              topologyKey: "kubernetes.io/hostname"
      imagePullSecrets:
{{ toYaml .Values.image.secrets | indent 10 }}
      containers:
      - name: cantaloupe
        image: pulibrary/cantaloupe
        imagePullPolicy: {{ .Values.image.pullPolicy }}
{{- if .Values.application.secretName }}
        envFrom:
        - secretRef:
            name: {{ .Values.application.secretName }}
{{- end }}
        ports:
        - containerPort: 8182
        resources:
          requests:
            cpu: 100m
            memory: 500Mi
        volumeMounts:
        - name: corpora-data
          mountPath: /var/lib/cantaloupe/images/corpora
          subPath: corpora
        - name: configmap-volume
          mountPath: /etc/cantaloupe.properties
          subPath: cantaloupe.properties
      volumes:
      - name: corpora-data
        persistentVolumeClaim:
          claimName: corpora-pvc
      - name: configmap-volume
        configMap:
          name: cantaloupe-prop
