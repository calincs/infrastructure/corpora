import json
from queries import *
from lookups import *
from indexBuilder import *


def main():
    sparql_endpoint = "https://bg-review.lincsproject.ca/sparql"
    sparql_user = "lincs"
    sparql_pw = "737e653a40"
    
    builder = Builder(sparql_endpoint, sparql_user, sparql_pw)
  
    graphs = builder.fetch(METADATA_QUERY)

    for graph in graphs:
        builder.actors(ACTOR_QUERY.replace("__GRAPH__", graph['iri']['value']))
        builder.things(OBJECT_QUERY.replace("__GRAPH__", graph['iri']['value']))
        builder.thingCreators(OBJECT_CREATORS_QUERY.replace("__GRAPH__", graph['iri']['value']))
        builder.thingDates(OBJECT_DATE_QUERY.replace("__GRAPH__", graph['iri']['value']))

    builder.actorBirths(BIRTH_QUERY)
    builder.actorDeaths(DEATH_QUERY)
    builder.actorRoles(ROLE_QUERY)
    builder.actorAttributes(ATTRIBUTE_QUERY)
    builder.actorGroupConnections(GROUP_CONNECTIONS_QUERY)
    builder.actorPlaces(ACTOR_PLACE_QUERY)
    builder.transformActorPlaces()
    builder.transformActorDates()
    builder.transformThingDates()
    builder.addIncomingPersonCounts()
    builder.addIncomingThingCounts()
    builder.deduplicate()
    with open('actors.json', 'w', encoding='utf-8') as f:
        json.dump(builder.actorData, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    main()
