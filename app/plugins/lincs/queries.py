METADATA_QUERY = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
        SELECT distinct ?iri ?name ?short_name ?home WHERE { graph <http://metadata.lincsproject.ca> {
        ?iri a <http://rdfs.org/ns/void#Dataset> ; 
        <http://purl.org/dc/terms/title> ?name ;
        <http://purl.org/dc/terms/alternative> ?short_name ;
        crm:P1_is_identified_by ?identifier .
        ?identifier crm:P2_has_type <http://id.lincsproject.ca/home> ;
        crm:P190_has_symbolic_content ?home
        } }"""

RDF_TYPE_QUERY = """
  PREFIX graph: <http://graph.lincsproject.ca/>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
  PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
  PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
  SELECT ?class ?label ?comment WHERE {  GRAPH <http://www.cidoc-crm.org/cidoc-crm/context> {   
    ?class a <http://www.w3.org/2000/01/rdf-schema#Class> ;
      rdfs:label ?label ;
      rdfs:comment ?comment .
      FILTER(lang(?label)="en")
    }
  }
"""

E55_TYPE_QUERY = """
  PREFIX graph: <http://graph.lincsproject.ca/>
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
  PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
  PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
  SELECT DISTINCT ?iri ?label WHERE {  
    ?iri a crm:E55_Type ;
    rdfs:label ?label .
    FILTER(lang(?label)="en")
    }
"""


### ACTOR QUERIES

ACTOR_QUERY = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
SELECT ?graph ?actor ?class ?class_label ?label ?alt_label ?image_iri ?note ?affiliated_groups ?child ?parent WHERE {  GRAPH <__GRAPH__> {   
      ?actor a ?class ;
      rdfs:label ?label .
      FILTER(?class in (<http://www.cidoc-crm.org/cidoc-crm/E21_Person>, <http://www.cidoc-crm.org/cidoc-crm/E74_Group>, <http://www.cidoc-crm.org/cidoc-crm/E39_Actor>))
      OPTIONAL {?actor <http://www.cidoc-crm.org/cidoc-crm/P138i_has_representation> ?image_iri}
      OPTIONAL {?actor skos:altLabel ?alt_label } 
      OPTIONAL {?actor crm:P3_has_note ?note }
      OPTIONAL {?actor <http://www.cidoc-crm.org/cidoc-crm/P107i_is_current_or_former_member_of> ?affiliated_groups } 
      BIND(<__GRAPH__> as ?graph)
	}
    ?class rdfs:label ?class_label
    FILTER(lang(?class_label) = 'en')
}
"""

GROUP_CONNECTIONS_QUERY = """
PREFIX graph: <http://graph.lincsproject.ca/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
select distinct ?person ?event ?group where {
  	?event a ?type .
  	?type rdfs:subClassOf* crm:E5_Event .
  	?event ?p1 ?group .
  	?group a crm:E74_Group .
  	?person ?p2 ?event .
  	?person a crm:E21_Person .
    FILTER(NOT EXISTS {?group a <http://www.cidoc-crm.org/cidoc-crm/E21_Person>})
  	FILTER(?group!=<http://temp.lincsproject.ca/Miles_Davis_Quintet>)
}
"""

BIRTH_QUERY = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
SELECT ?person ?birth_start ?birth_end ?birth_place ?birth_place_label ?birth_place_geo_coordinates ?participants ?notes  WHERE {       
        {   
          ?person a crm:E21_Person ;
          <http://www.cidoc-crm.org/cidoc-crm/P98i_was_born> ?birth . 
          OPTIONAL {
            ?birth <http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span> ?birth_date .
            OPTIONAL { ?birth_date <http://www.cidoc-crm.org/cidoc-crm/P82a_begin_of_the_begin> ?birth_start }
            OPTIONAL { ?birth_date <http://www.cidoc-crm.org/cidoc-crm/P82b_end_of_the_end> ?birth_end }
          }
          OPTIONAL { 
            ?birth <http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at> ?place . 
  		      ?place <http://www.cidoc-crm.org/cidoc-crm/P89_falls_within> ?birth_place .
    		?birth_place rdfs:label ?birth_place_label .
          OPTIONAL { ?birth_place <http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> ?birth_place_geo_coordinates . }
          } 
        } UNION
        {
    	  ?birth <http://www.cidoc-crm.org/cidoc-crm/P98_brought_into_life> ?person 
          OPTIONAL {
            ?birth <http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span> ?birth_date .
            OPTIONAL { ?birth_date <http://www.cidoc-crm.org/cidoc-crm/P82a_begin_of_the_begin> ?birth_start }
            OPTIONAL { ?birth_date <http://www.cidoc-crm.org/cidoc-crm/P82b_end_of_the_end> ?birth_end }
          }
          OPTIONAL { 
            ?birth <http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at> ?birth_place .
    	    ?birth_place rdfs:label ?birth_place_label .
  		    OPTIONAL { ?birth_place <http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> ?birth_place_geo_coordinates . }
	      }
          
 	      OPTIONAL { ?birth <http://www.cidoc-crm.org/cidoc-crm/P3_has_note> ?notes } .
        }       
}
"""
DEATH_QUERY = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
SELECT * WHERE {
        { 
          ?person a crm:E21_Person ;
          <http://www.cidoc-crm.org/cidoc-crm/P100i_died_in> ?death . 
          ?death <http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span> ?death_time .
          OPTIONAL { ?death_time <http://www.cidoc-crm.org/cidoc-crm/P82a_begin_of_the_begin> ?death_start }
          OPTIONAL { ?death_time <http://www.cidoc-crm.org/cidoc-crm/P82b_end_of_the_end> ?death_end }
          OPTIONAL { 
            ?death <http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at> ?place . 
  		    ?place <http://www.cidoc-crm.org/cidoc-crm/P89_falls_within> ?death_place .
    		?death_place rdfs:label ?death_place_label .
          OPTIONAL { ?death_place <http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> ?death_place_geo_coordinates . }
          } 
        } UNION
        {
    	  ?death <http://www.cidoc-crm.org/cidoc-crm/P100_was_death_of> ?person 
          OPTIONAL {
            ?death <http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span> ?death_date .
            OPTIONAL { ?death_date <http://www.cidoc-crm.org/cidoc-crm/P82a_begin_of_the_begin> ?death_start }
            OPTIONAL { ?death_date <http://www.cidoc-crm.org/cidoc-crm/P82b_end_of_the_end> ?death_end }
          }
          OPTIONAL { 
            ?death <http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at> ?death_place .
    	    ?death_place rdfs:label ?death_place_label .
  		    OPTIONAL { ?death_place <http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> ?death_place_geo_coordinates . }
	      }      
        }
}
"""

ATTRIBUTE_QUERY = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
select ?person ?assignment ?attribute ?note where {
    ?assignment <http://www.cidoc-crm.org/cidoc-crm/P140_assigned_attribute_to> ?person ;
    crm:P2_has_type ?context ;
    rdfs:label ?assignment_label ;
    crm:P141_assigned ?assigned . 
    ?context rdfs:label ?context_label .
    ?assigned crm:P16_used_specific_thing ?thing .
    ?thing rdfs:label ?thing_label .
    OPTIONAL {?assignment crm:P3_has_note ?note}
    FILTER(lang(?thing_label) = 'en')
    FILTER(lang(?context_label) = 'en')
    BIND (CONCAT(CONCAT(?context_label, ': '), ?thing_label) as ?attribute)
} 
"""

CONNECTED_OBJECT_QUERY = """
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
select distinct ?person ?thing where {
  {
      ?range <http://www.cidoc-crm.org/cidoc-crm/P02_has_range> ?person ;
      <http://www.cidoc-crm.org/cidoc-crm/P14.1_in_the_role_of> ?role .
      ?person a crm:E21_Person .
      ?creation <http://www.cidoc-crm.org/cidoc-crm/P01i_is_domain_of> ?range .
      ?thing <http://www.cidoc-crm.org/cidoc-crm/P94i_was_created_by> ?creation ;

  } UNION {
      ?event <http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by> ?person .
      ?person a crm:E21_Person .
      ?event a ?type .
      ?thing ?p ?event ;
      FILTER(?type in (crm:E12_Production, crm:E65_Creation, frbroo:F31_Performance))
  } 
}
"""

ROLE_QUERY = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> 
SELECT distinct ?person ?role ?role_label ?type_label ?technique_label WHERE {
  ?person a crm:E21_Person ;
  <http://www.cidoc-crm.org/cidoc-crm/P14i_performed> ?role .
  ?role rdfs:label ?role_label ;
  a <http://iflastandards.info/ns/fr/frbr/frbroo/F51_Pursuit> .
  OPTIONAL {
          ?role <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> ?role_type .
          ?role_type rdfs:label ?type_label .
          FILTER(?type_label != "")
          FILTER(lang(?type_label) != "fr")
          FILTER(?role_type != <http://id.lincsproject.ca/cwrc#Occupation>)
          FILTER(?role_type != <http://id.lincsproject.ca/cwrc/Occupation>)
          FILTER(?role_type != <http://id.lincsproject.ca/occupation/Occupation>)
  }
  OPTIONAL {
          ?role <http://www.cidoc-crm.org/cidoc-crm/P32_used_general_technique>|<http://www.cidoc-crm.org/cidoc-crm/P125_used_thing_of_type> ?technique .
          ?technique rdfs:label ?technique_label .
  }
}"""


ACTOR_PLACE_QUERY = """
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Select distinct ?person ?place ?place_label ?place_geo_coordinates where {
  ?place rdfs:label ?place_label .     

  OPTIONAL {  ?place <http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> ?place_geo_coordinates .  }
  {	
  select distinct ?person ?place where {
      {
          ?person a crm:E21_Person .
          ?event ?predicate ?person ;
          a ?type ;
          ?p ?place .
          ?place a crm:E53_Place .
          ?type rdfs:subClassOf* crm:E5_Event .
          FILTER(NOT EXISTS {?event a <http://www.cidoc-crm.org/cidoc-crm/E69_Death>})
  		  FILTER(NOT EXISTS {?event a <http://www.cidoc-crm.org/cidoc-crm/E67_Birth>})
      } UNION
      {
          ?person a crm:E21_Person ;
          ?predicate ?event .
          ?event a ?type ;
          ?p ?place .
          ?place a crm:E53_Place .
          ?type rdfs:subClassOf* crm:E5_Event
          FILTER(NOT EXISTS {?event a <http://www.cidoc-crm.org/cidoc-crm/E69_Death>})
  		  FILTER(NOT EXISTS {?event a <http://www.cidoc-crm.org/cidoc-crm/E67_Birth>})
      } UNION {
          ?person a crm:E21_Person ;
          ?p ?place .
          ?place a crm:E53_Place . 
      } UNION {
          ?person a crm:E21_Person .
          ?place a crm:E53_Place ;
          ?p ?person .
      }
      }}
}"""


############## OBJECTS QUERIES #####################

OBJECT_QUERY = """SELECT ?thing ?class_label ?graph ?label ?image_iri ?note ?type_label ?topic_label ?material_label ?owner ?owner_label ?owner_class_label WHERE {  
  GRAPH <__GRAPH__> { 
    ?thing a ?class 
    FILTER(?class in (<http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object>,<http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work>,<http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression>))
  } .  
  ?class rdfs:label ?class_label .
  ?thing rdfs:label|<https://www.w3.org/2004/02/skos/core#altLabel>|<https://www.w3.org/2004/02/skos/core#prefLabel> ?label .  
  FILTER(lang(?class_label) = 'en')
  OPTIONAL {?thing <http://www.cidoc-crm.org/cidoc-crm/P138i_has_representation> ?image_iri}
  OPTIONAL { ?thing <http://www.cidoc-crm.org/cidoc-crm/P3_has_note> ?note}  
  OPTIONAL { ?thing <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> ?type .  ?type rdfs:label ?type_label }  
  OPTIONAL {  ?thing <http://www.cidoc-crm.org/cidoc-crm/P129_is_about> ?topic .  ?topic rdfs:label ?topic_label  }  
  OPTIONAL {  ?thing <http://www.cidoc-crm.org/cidoc-crm/P45_consists_of> ?material .  ?material rdfs:label ?material_label .  }  
  OPTIONAL {  ?thing <http://www.cidoc-crm.org/cidoc-crm/P52_has_current_owner>|<http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner> ?owner .} 
  BIND(<__GRAPH__> as ?graph)
}"""

OBJECT_DATE_QUERY = """SELECT ?thing ?date WHERE { 
    GRAPH <__GRAPH__> { ?thing a ?class } .  
    FILTER(?class in (<http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object>,<http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work>,<http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression>)) 
    ?thing rdfs:label ?thing_label .
    ?thing <http://www.cidoc-crm.org/cidoc-crm/P94i_was_created_by>|<http://www.cidoc-crm.org/cidoc-crm/P108i_was_produced_by> ?event . 
    ?event <http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span> ?time .  
    ?time <http://www.cidoc-crm.org/cidoc-crm/P82a_begin_of_the_begin> ?date .
    }""" 

OBJECT_PLACE_QUERY = """SELECT ?thing ?place ?place_label ?place_geo_coordinates WHERE { 
    GRAPH <__GRAPH__> { ?thing a ?class } . 
    FILTER(?class in (<http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object>,<http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work>,<http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression>))
    ?thing <http://www.cidoc-crm.org/cidoc-crm/P94i_was_created_by>|<http://www.cidoc-crm.org/cidoc-crm/P108i_was_produced_by> ?event . 
    ?thing rdfs:label ?thing_label .
    ?event <http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at> ?place . 
    ?place rdfs:label ?place_label .  
    OPTIONAL {  ?place <http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> ?place_geo_coordinates .  }
    }""" 

OBJECT_CREATORS_QUERY = """PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT distinct ?thing ?creator ?creator_label ?class_label WHERE { 
  GRAPH <__GRAPH__> { ?thing a ?class } . 
  FILTER(?class in (<http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object>,<http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work>,<http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression>)) 
  ?thing rdfs:label ?thing_label .
  ?thing <http://www.cidoc-crm.org/cidoc-crm/P94i_was_created_by>|<http://www.cidoc-crm.org/cidoc-crm/P108i_was_produced_by> ?event . 
  OPTIONAL {  ?event <http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by> ?creator . 
    ?creator rdfs:label ?creator_label ;
  	rdf:type ?creator_class . 
    ?creator_class rdfs:label ?class_label . 
  	FILTER(lang(?class_label)='en')
  } 
  OPTIONAL { 
    ?event crm:P01i_is_domain_of ?role . 
    ?role crm:P02_has_range ?creator .
    ?creator rdfs:label ?creator_label ; 
             rdf:type ?creator_class . 
    ?creator_class rdfs:label ?class_label .
    FILTER(lang(?class_label)='en')
  }
  FILTER(!isBlank(?creator))
}"""


