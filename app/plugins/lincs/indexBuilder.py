from SPARQLWrapper import SPARQLWrapper, JSON, POST
from rdflib import Graph
import matplotlib.path as matplot_path
try:
    from corpus import *
except ImportError:
    pass


class Builder:

    def __init__(self, sparql_endpoint, sparql_user, sparql_pw):
        self.sparql_endpoint = sparql_endpoint
        self.sparql_user = sparql_user
        self.sparql_pw = sparql_pw
        self.thingData = {}
        self.actorData = {}
        self.placeData = {} 


    def fetch(self, query):
        sparql = SPARQLWrapper(self.sparql_endpoint)
        sparql.setCredentials(self.sparql_user, self.sparql_pw)
        sparql.setReturnFormat(JSON)
        sparql.setMethod(POST)
        sparql.setQuery(query)
        try:
            return sparql.queryAndConvert()["results"]["bindings"]
        except Exception as e:
            print(e)

    def actors(self, query):
        for actor in self.fetch(query):
            if actor['actor']['value'] in self.actorData:
                self.addActorData(actor)
            else:
                self.actorData[actor['actor']['value']] = {
                    "graphs": [],
                    "names": [],
                    "classes": [],
                    "affiliated_groups": [],
                    "notes": [],
                    "attributes": [],
                    "professions": [],
                    "techniques": [],
                    "places": {},
                    "things": [],
                    "images": []
                }
                self.addActorData(actor)

    def addActorData(self, actor):
        if actor['graph']['value'] not in self.actorData[actor['actor']['value']]['graphs']:
            self.actorData[actor['actor']['value']]['graphs'].append(actor['graph']['value'])

        if actor['label']['value'] not in self.actorData[actor['actor']['value']]['names']:
            self.actorData[actor['actor']['value']]['names'].append(actor['label']['value'])
        
        if 'class_label' in actor:
            if actor['class']['value'] not in self.actorData[actor['actor']['value']]['classes']:
                self.actorData[actor['actor']['value']]['classes'].append(actor['class_label']['value'])
        
        if 'alt_label' in actor:
            if actor['alt_label']['value'] not in self.actorData[actor['actor']['value']]['names']:
                self.actorData[actor['actor']['value']]['names'].append(actor['alt_label']['value'])

        if 'note' in actor:
            if actor['note']['value'] not in self.actorData[actor['actor']['value']]['notes']:
                self.actorData[actor['actor']['value']]['notes'].append(actor['note']['value'])

        if 'affiliated_groups' in actor:
            if actor['affiliated_groups']['value'] not in self.actorData[actor['actor']['value']]['affiliated_groups']:
                self.actorData[actor['actor']['value']]['affiliated_groups'].append(actor['affiliated_groups']['value'])

        if 'image_iri' in actor:
            if actor['image_iri']['value'] not in self.actorData[actor['actor']['value']]['images']:
                self.actorData[actor['actor']['value']]['images'].append(actor['image_iri']['value'])


    def actorGroupConnections(self, query):
        for group in self.fetch(query):
            if group['person']['value'] in self.actorData:
                if group['group'] not in self.actorData[group['person']['value']]['affiliated_groups']:
                    self.actorData[group['person']['value']]['affiliated_groups'].append(group['group']['value'])


    def actorAttributes(self, query):
        for attribute in self.fetch(query):
            if attribute['person']['value'] in self.actorData:
                if attribute['attribute']['value'] not in self.actorData[attribute['person']['value']]['attributes']:
                    self.actorData[attribute['person']['value']]['attributes'].append(attribute['attribute']['value'])
                    self.actorData[attribute['person']['value']]['attributes'] = list(set(self.actorData[attribute['person']['value']]['attributes']))


    def actorBirths(self, query):           
        for birth in self.fetch(query):
            if "birth_start" in birth:
                self.actorData[birth['person']['value']]["birth_date_start"] = birth['birth_start']['value']
            if "birth_end" in birth:
                self.actorData[birth['person']['value']]["birth_date_end"] = birth['birth_end']['value']
            if "birth_place_label" in birth:
                self.actorData[birth['person']['value']]["birth_place"] = {
                    birth["birth_place"]['value']: {
                        "label": birth['birth_place_label']['value']
                    }
                }
            if "birth_place_geo_coordinates" in birth:
                try:
                    self.actorData[birth['person']['value']]["birth_place"][birth["birth_place"]['value']]['geo_coordinates'] = {"type": birth['birth_place_geo_coordinates']['value'].split("(")[0], "coordinates": [[float(p) for p in i.split(" ")] for i in birth['birth_place_geo_coordinates']['value'].split("(")[1].strip(")").split(",")]} 
                    if len(self.actorData[birth['person']['value']]["birth_place"][birth["birth_place"]['value']]['geo_coordinates']['coordinates']) == 1:
                        self.actorData[birth['person']['value']]["birth_place"][birth["birth_place"]['value']]['geo_coordinates']["coordinates"] = self.actorData[birth['person']['value']]["birth_place"][birth["birth_place"]['value']]['geo_coordinates']['coordinates'][0]
                except Exception as e:
                    print(birth["birth_place"]['value'], e)
            if "notes" in birth:
                self.actorData[birth['person']['value']]['notes'].append(birth['notes']['value'])


    def actorDeaths(self, query):
        for death in self.fetch(query):
            if "death_start" in death:
                self.actorData[death['person']['value']]["death_date_start"] = death['death_start']['value']
            if "death_end" in death:
                self.actorData[death['person']['value']]["death_date_end"] = death['death_end']['value']
            if "death_place_label" in death:
                self.actorData[death['person']['value']]["death_place"] = {
                    death["death_place"]['value']: {
                        "label": death['death_place_label']['value']
                    }
                }
            if "death_place_geo_coordinates" in death:
                try:
                    self.actorData[death['person']['value']]["death_place"][death["death_place"]['value']]['geo_coordinates'] = {"type": death['death_place_geo_coordinates']['value'].split("(")[0], "coordinates": [[float(p) for p in i.split(" ")] for i in death['death_place_geo_coordinates']['value'].split("(")[1].strip(")").split(",")]} 
                    if len(self.actorData[death['person']['value']]["death_place"][death["death_place"]['value']]['geo_coordinates']['coordinates']) == 1:
                        self.actorData[death['person']['value']]["death_place"][death["death_place"]['value']]['geo_coordinates']['coordinates'] = self.actorData[death['person']['value']]["death_place"][death["death_place"]['value']]['geo_coordinates']['coordinates'][0]
                except Exception as e:
                    print(death["death_place"]['value'], e)


    def actorRoles(self, query):
        for role in self.fetch(query):           
            if "type_label" in role:
                if role['type_label']!='':
                    self.actorData[role['person']['value']]['professions'].append(role['type_label']['value'])
            if "technique_label" in role:
                self.actorData[role['person']['value']]['techniques'].append(role['technique_label']['value'])


    def actorPlaces(self, query):
        for place in self.fetch(query):
            self.actorData[place['person']['value']]["places"][place["place"]['value']] = {
                "label": place['place_label']['value']
            }
            if "place_geo_coordinates" in place:
                try:
                    self.actorData[place['person']['value']]["places"][place["place"]['value']]['geo_coordinates'] = {"type": place['place_geo_coordinates']['value'].split("(")[0], "coordinates": [[float(p) for p in i.split(" ")] for i in place['place_geo_coordinates']['value'].split("(")[1].strip(")").split(",")]} 
                    if len(self.actorData[place['person']['value']]["places"][place["place"]['value']]['geo_coordinates']['coordinates']) == 1:
                        self.actorData[place['person']['value']]["places"][place["place"]['value']]['geo_coordinates']['coordinates'] = self.actorData[place['person']['value']]["places"][place["place"]['value']]['geo_coordinates']['coordinates'][0]
                except Exception as e:
                    print(place["place"]['value'], e)       


    def actorThingConnections(self, query):
        for thing in self.fetch(query):
            if thing['thing']['value'] not in self.actorData[thing['person']['value']]['things']:
                self.actorData[thing['person']['value']]['things'].append(thing['thing']['value'])


    ### ACTOR TRANSFORMATIONS

    def transformActorDates(self):
        for key in self.actorData.keys():
            if "birth_date_start" in self.actorData[key]:
                try:
                    self.actorData[key]["birth_year"] = int(str(self.actorData[key]["birth_date_start"])[:5].rstrip("-"))
                except Exception as e:
                    print(e)
                    pass
                try:
                    self.actorData[key]["birth_date"] = str(self.actorData[key]["birth_date_start"]).split("T")[0]
                except Exception as e:
                    print(e)
                    pass
            if "birth_date_end" in self.actorData[key]:
                try:
                    self.actorData[key]["birth_year"] = int(str(self.actorData[key]["birth_date_end"])[:5].rstrip("-"))
                except Exception as e:
                    print(e)
                    pass
                try:
                    self.actorData[key]["birth_date"] = str(self.actorData[key]["birth_date_end"]).split("T")[0]
                except Exception as e:
                    print(e)
                    pass
            if "death_date_start" in self.actorData[key]:
                try:
                    self.actorData[key]["death_year"] = int(str(self.actorData[key]["death_date_start"])[:5].rstrip("-"))
                except Exception as e:
                    print(e)
                    pass
                try:
                    self.actorData[key]["death_date"] = str(self.actorData[key]["death_date_start"]).split("T")[0]
                except Exception as e:
                    print(e)
                    pass
            if "death_date_end" in self.actorData[key]:
                try:
                    self.actorData[key]["death_year"] = int(str(self.actorData[key]["death_date_end"])[:5].rstrip("-"))
                except Exception as e:
                    print(e)
                    pass
                try:
                    self.actorData[key]["death_date"] = str(self.actorData[key]["death_date_end"]).split("T")[0]
                except Exception as e:
                    print(e)
                    pass
            if ("death_year" in self.actorData[key]) & ("birth_year" in self.actorData[key]):
                self.actorData[key]['years_active'] = list(range(self.actorData[key]['birth_year'], self.actorData[key]['death_year']+1))


    def transformActorPlaces(self):
        for key in self.actorData.keys():
            if "birth_place" in self.actorData[key]:
                for place_key in self.actorData[key]['birth_place'].keys():
                    if place_key not in self.placeData.keys():
                        self.placeData[place_key] = self.actorData[key]['birth_place'][place_key]
            if "death_place" in self.actorData[key]:
                for place_key in self.actorData[key]['death_place'].keys():
                    if place_key not in self.placeData.keys():
                        self.placeData[place_key] = self.actorData[key]['death_place'][place_key]
            if "places" in self.actorData[key]:
                for place_key in self.actorData[key]['places'].keys():
                    if place_key not in self.placeData.keys():
                        self.placeData[place_key] = self.actorData[key]['places'][place_key]


    def addIncomingPersonCounts(self):
        chunk_size = 500
        persons_chunked = [list(self.actorData.keys())[i * chunk_size:(i + 1) * chunk_size] for i in range((len(self.actorData.keys()) + chunk_size - 1) // chunk_size )] 
        for chunk in persons_chunked:
            CHUNKED_QUERY = "SELECT distinct ?iri (COUNT(distinct ?subject) AS ?count) where { ?subject ?predicate ?iri VALUES (?iri) { IRI_VALUES } } GROUP BY ?iri".replace("IRI_VALUES", " ".join(["(<{}>)".format(item) for item in chunk]))       
            count_data = self.fetch(CHUNKED_QUERY)
            for count in count_data:
                self.actorData[count['iri']['value']]['incoming_count'] = int(count['count']['value'])
        for key in self.actorData.keys():
            if 'incoming_count' not in self.actorData[key]:
                self.actorData[key]['incoming_count'] = 0

    def addIncomingThingCounts(self):
        chunk_size = 500
        things_chunked = [list(self.thingData.keys())[i * chunk_size:(i + 1) * chunk_size] for i in range((len(self.thingData.keys()) + chunk_size - 1) // chunk_size )] 
        for chunk in things_chunked:
            CHUNKED_QUERY = "SELECT distinct ?iri (COUNT(distinct ?subject) AS ?count) where { ?subject ?predicate ?iri VALUES (?iri) { IRI_VALUES } } GROUP BY ?iri".replace("IRI_VALUES", " ".join(["(<{}>)".format(item) for item in chunk]))       
            count_data = self.fetch(CHUNKED_QUERY)
            for count in count_data:
                self.thingData[count['iri']['value']]['incoming_count'] = int(count['count']['value'])
        for key in self.thingData.keys():
            if 'incoming_count' not in self.thingData[key]:
                self.thingData[key]['incoming_count'] = 0
                
              

    ### OBJECT PARSING

    def things(self, query):
        thing_data = self.fetch(query)
    
        for thing in thing_data:
            if thing['thing']['value'] in self.thingData:
                self.addThingData(thing)
            else:
                self.thingData[thing['thing']['value']] = {
                    "graphs": [],
                    "classes": [],
                    "names": [],
                    "notes": [],
                    "creators": [],
                    "owners": [],
                    "places": [],
                    "topics": [],
                    "date": [],
                    "types": [],
                    "materials": [],
                    "images": []
                }
                self.addThingData(thing)    


    def addThingData(self, thing):
        if thing['graph']['value'] not in self.thingData[thing['thing']['value']]['graphs']:
            self.thingData[thing['thing']['value']]['graphs'].append(thing['graph']['value'])

        if thing['label']['value'] not in self.thingData[thing['thing']['value']]['names']:
            self.thingData[thing['thing']['value']]['names'].append(thing['label']['value'])

        if thing['class_label']['value'] not in self.thingData[thing['thing']['value']]['classes']:
            self.thingData[thing['thing']['value']]['classes'].append(thing['class_label']['value'])
        
        if 'note' in thing:
            if thing['note']['value'] not in self.thingData[thing['thing']['value']]['notes']:
                self.thingData[thing['thing']['value']]['notes'].append(thing['note']['value'])

        if 'type_label' in thing:
            if thing['type_label']['value'] not in self.thingData[thing['thing']['value']]['types']:
                self.thingData[thing['thing']['value']]['types'].append(thing['type_label']['value'])

        if 'material_label' in thing:
            if thing['material_label']['value'] not in self.thingData[thing['thing']['value']]['materials']:
                self.thingData[thing['thing']['value']]['materials'].append(thing['material_label']['value'])

        if 'topic_label' in thing:
            if thing['topic_label']['value'] not in self.thingData[thing['thing']['value']]['topics']:
                self.thingData[thing['thing']['value']]['topics'].append(thing['topic_label']['value'])
        
        if 'owner' in thing:
            if thing['owner']['value'] not in self.thingData[thing['thing']['value']]['owners']:
                self.thingData[thing['thing']['value']]['owners'].append(thing['owner']['value'])

        if 'image_iri' in thing:
            if thing['image_iri']['value'] not in self.thingData[thing['thing']['value']]['images']:
                self.thingData[thing['thing']['value']]['images'].append(thing['image_iri']['value'])

            
    def thingDates(self, query):
        date_data = self.fetch(query)
        
        for date in date_data:
            if 'date' in date:
                if date['date']['value'] not in self.thingData[date['thing']['value']]['date']:
                    self.thingData[date['thing']['value']]['date'].append(date['date']['value'])

    def thingPlaces(self, query):
        place_data = self.fetch(query)
        
        for place in place_data:
            if 'place' in place:
                if place['place']['value'] not in self.thingData[place['thing']['value']]['place']:
                    self.thingData[place['thing']['value']]['place'].append(place['place']['value'])
                if place['place']['value'] not in self.placeData:
                    self.placeData[place['place']['value']] = {"label": place['place_label']['value']}
                    if 'place_geo_coordinates' in place:
                        self.placeData[place['place']['value']]['geo_coordinates'] = {"type": place['place_geo_coordinates']['value'].split("(")[0], "coordinates": [[float(p) for p in i.split(" ")] for i in place['place_geo_coordinates']['value'].split("(")[1].strip(")").split(",")]} 
                        
        

    def thingCreators(self, query):
        creator_data = self.fetch(query)

        for creator in creator_data:
            if 'creator' in creator:
                if creator['creator']['value'] not in self.thingData[creator['thing']['value']]['creators']:
                    self.thingData[creator['thing']['value']]['creators'].append(creator['creator']['value'])
            
                if creator['creator']['value'] not in self.actorData:
                    self.actorData[creator['creator']['value']] = {"labels": [creator['creator_label']['value']], "type": creator['class_label']['value']}
                else:
                    if creator['creator_label']['value'] not in self.actorData[creator['creator']['value']]['names']:
                        self.actorData[creator['creator']['value']]['names'].append(creator['creator_label']['value'])

    ### OBJECT TRANSFORMATIONS

    def transformThingDates(self):
        for key in self.thingData.keys():
            if "date" in self.thingData[key]:
                if len(self.thingData[key]["date"])>0:
                    try:
                        self.thingData[key]["date"][0] = int(str(self.thingData[key]["date"][0])[:5].rstrip("-"))
                    except Exception as e:
                        pass
                    try:
                        self.thingData[key]["date"][0] = str(self.thingData[key]["date"][0]).split("T")[0]
                    except Exception as e:
                        pass

    def transformThingPlaceData(self):
        for key in self.thingData.keys():
            if "place" in self.thingData[key]:
                for place_key in self.thingData[key]['place'].keys():
                    if place_key not in self.placeData:
                        self.placeData[place_key] = self.thingData[key]['place'][place_key]


    ### MISC

    def deduplicate(self):
        for key in self.thingData:
            for field in self.thingData[key]:
                if isinstance(self.thingData[key][field], list):
                    self.thingData[key][field] = list(set(self.thingData[key][field]))
        for key in self.actorData:
            for field in self.actorData[key]:
                if isinstance(self.actorData[key][field], list):
                    self.actorData[key][field] = list(set(self.actorData[key][field]))


    
    def loadCountryShapes(self):
        country_shapes = None
        country_shapes_directory = os.path.dirname(os.path.abspath(__file__))
        country_shapes_file = f"{country_shapes_directory}/countries.geo.json"

        with open(country_shapes_file, 'r', encoding='utf-8') as country_shapes_in:
            country_shapes = json.load(country_shapes_in)

        return country_shapes


    def getCountry(self, coordinates, shapes, corpus):
        country_found = False
        if coordinates:
            for country in shapes['features']:
                geometry = country['geometry']['type']
                coords = country['geometry']['coordinates']
                if geometry == 'Polygon':
                    coords = [coords]

                for polygon in coords:
                    shape = matplot_path.Path(polygon[0], closed=True)
                    country_found = shape.contains_point(coordinates)

                    if country_found and len(polygon) > 1:
                        for hole_index in range(1, len(polygon)):
                            hole = matplot_path.Path(polygon[hole_index], closed=True)
                            if hole.contains_point(coordinates):
                                country_found = False
                                break

                    if country_found:
                        return corpus.get_or_create_content('Country', {'name': country['properties']['name']}, use_cache=True)
                        #return country['properties']['name']

        return None