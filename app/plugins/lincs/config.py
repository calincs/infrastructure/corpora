
ROLE_QUERY = """
SELECT distinct ?role ?role_label ?type_label ?technique_label ?participant_name WHERE {
        ?person a crm:E21_Person ;
        <http://www.cidoc-crm.org/cidoc-crm/P14i_performed> ?role .
  	?role rdfs:label ?role_label
        OPTIONAL {
                ?role <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> ?role_type .
                ?role_type rdfs:label ?type_label .
  				FILTER(?role_type != <http://id.lincsproject.ca/cwrc#Occupation>)
        }
        OPTIONAL {
                ?role <http://www.cidoc-crm.org/cidoc-crm/P32_used_general_technique>|<http://www.cidoc-crm.org/cidoc-crm/P125_used_object_of_type> ?technique .
                ?technique rdfs:label ?technique_label
        }
		OPTIONAL {
                ?role <http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by> ?participant .
                ?participant rdfs:label ?participant_name .
                FILTER (?person != ?participant)
        }
}       
"""

BIRTH_QUERY = """
SELECT * WHERE { 
        ?person a crm:E21_Person ;
        <http://www.cidoc-crm.org/cidoc-crm/P98i_was_born> ?birth . 
        ?birth <http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span> ?birth_time .
        OPTIONAL { ?birth_time <http://www.cidoc-crm.org/cidoc-crm/P82_at_some_time_within> ?birth_within }
        OPTIONAL { ?birth_time <http://www.cidoc-crm.org/cidoc-crm/P82a_begin_of_the_begin> ?birth_start }
        OPTIONAL { ?birth_time <http://www.cidoc-crm.org/cidoc-crm/P82b_end_of_the_end> ?birth_end }
        OPTIONAL { ?birth <http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at> ?birth_place . ?birth_place rdfs:label ?birth_place_label}
}
"""

DEATH_QUERY = """
SELECT * WHERE { 
        ?person a crm:E21_Person ;
        <http://www.cidoc-crm.org/cidoc-crm/P100i_died_in> ?death . 
        ?death <http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span> ?death_time .
        OPTIONAL { ?death_time <http://www.cidoc-crm.org/cidoc-crm/P82_at_some_time_within> ?death_within }
        OPTIONAL { ?death_time <http://www.cidoc-crm.org/cidoc-crm/P82a_begin_of_the_begin> ?death_start }
        OPTIONAL { ?death_time <http://www.cidoc-crm.org/cidoc-crm/P82b_end_of_the_end> ?death_end }
        OPTIONAL { ?death <http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at> ?death_place . ?death_place rdfs:label ?death_place_label}
}
"""


PERSON_QUERY = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> SELECT * WHERE { graph ?graph { ?person a crm:E21_Person ; rdfs:label ?label . OPTIONAL {?person skos:altLabel ?alt_label} OPTIONAL {?person crm:P3_has_note ?note}} OPTIONAL {?person <http://www.cidoc-crm.org/cidoc-crm/P107i_is_current_or_former_member_of> ?affiliation . ?affiliation rdfs:label ?affiliation_label .} OPTIONAL { ?person <http://www.cidoc-crm.org/cidoc-crm/P152i_is_parent_of> ?child . ?child rdfs:label ?child_name } OPTIONAL { ?person <http://www.cidoc-crm.org/cidoc-crm/P152_has_parent> ?parent . ?parent rdfs:label ?parent_name } FILTER (strstarts(str(?graph), \"http://graph.lincsproject.ca/\"))}"


## metadata query provides rules for selecting named graphs against which to index
METADATA_QUERY = """SELECT distinct ?iri ?name ?short_name ?home WHERE { graph <http://metadata.lincsproject.ca> {
        ?iri a <http://rdfs.org/ns/void#Dataset> ; 
        <http://purl.org/dc/terms/title> ?name ;
        <http://purl.org/dc/terms/alternative> ?short_name ;
        crm:P1_is_identified_by ?identifier .
        ?identifier crm:P2_has_type <http://id.lincsproject.ca/home> ;
        crm:P190_has_symbolic_content ?home
        } }"""

E55_TYPE_QUERY = """SELECT distinct ?iri ?name WHERE { ?iri a <http://www.cidoc-crm.org/cidoc-crm/E55_Type> ; <http://www.w3.org/2000/01/rdf-schema#label> ?name }""" 


