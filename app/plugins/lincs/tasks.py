import warnings
from elasticsearch.exceptions import ElasticsearchWarning
import traceback
from corpus import *
from manager.utilities import order_content_schema
from django.utils.text import slugify
from timeit import default_timer as timer
from .content import *
from manager.utilities import _contains
from SPARQLWrapper import SPARQLWrapper, JSON, POST
from rdflib import Graph
from datetime import datetime
import dateutil.parser as dparser
from .queries import *
from .lookups import *
from .indexBuilder import *

# IF i need to update the parameters, be sure to change the version number to force recognition
REGISTRY = {
    "LINCS Index": {
        "version": "2.1.0",
        "jobsite_type": "HUEY",
        "track_provenance": True,
        "create_report": True,
        "content_type": "Corpus",
        "configuration": {
            "parameters": {
                "sparql_endpoint": {
                    "value": "",
                    "type": "text",
                    "label": "SPARQL Endpoint",
                    "note": "The endpoint against which to index."
                },
                "sparql_user": {
                    "value": "",
                    "type": "text",
                    "label": "SPARQL User",
                    "note": "User credential."
                },
                "sparql_password": {
                    "value": "",
                    "type": "text",
                    "label": "SPARQL Password",
                    "note": "Password credential."
                },
                "thing_limit": {
                    "value": "na",
                    "type": "text",
                    "label": "Limit",
                    "note": "Limit the number of objects indexed (for testing). Optional."
                }
            },
        },
        "module": 'plugins.lincs.tasks',
        "functions": ['index_LINCS']
    }
}


def index_LINCS(job_id):
    time_start = timer()
    job = Job(job_id)
    corpus = job.corpus
    sparql_endpoint = job.get_param_value('sparql_endpoint')
    sparql_user = job.get_param_value('sparql_user')
    sparql_pw = job.get_param_value('sparql_password')
    try:
        limit = int(job.get_param_value("thing_limit"))
    except:
        limit = "na"
        pass

    job.set_status('running')
    job.report('''Attempting indexing using SPARQL endpoint: {0}\n'''.format(sparql_endpoint))

    try:
        es_logger = logging.getLogger('elasticsearch')
        es_log_level = es_logger.getEffectiveLevel()
        es_logger.setLevel(logging.CRITICAL)
        warnings.simplefilter('ignore', ElasticsearchWarning)

        # delete and recreate LINCS content types
        ordered_lincs_schema = order_content_schema(CONTENT_SCHEMA)  # order types by dependency
        ordered_lincs_schema.reverse()  # reverse order because we're deleting
        for lincs_content_type in ordered_lincs_schema:
            if lincs_content_type["name"] in corpus.content_types:
                job.report("Dropping {} table.".format(lincs_content_type["name"]))
                corpus.delete_content_type(lincs_content_type["name"])
        ordered_lincs_schema.reverse()  # put back in dependency order for creating
        for lincs_content_type in ordered_lincs_schema:
            if lincs_content_type["name"] not in corpus.content_types:
                job.report("Creating new {} table.".format(lincs_content_type["name"]))
                corpus.save_content_type(lincs_content_type)

        ingest(sparql_endpoint, corpus, job, sparql_user, sparql_pw, limit)

        job.set_status('running', percent_complete=42)
        time_stop = timer()
        job.report("\n\nLINCS persons ingestion completed in {0} seconds.".format(int(time_stop - time_start)))
        job.complete(status='complete')
        es_logger.setLevel(es_log_level)
    except:
        job.report("\n\nA major error prevented the ingestion of LINCS data:\n{0}".format(
            traceback.format_exc()
        ))
        job.complete(status='error')


def ingest(sparql_endpoint, corpus, job, sparql_user, sparql_pw, limit):
    
    builder = Builder(sparql_endpoint, sparql_user, sparql_pw)
    graph_cache = {}
    place_cache = {}
    country_shapes = builder.loadCountryShapes()
    missing_types = []
    missing_materials = []
    group_cache = {}
    person_cache = {}
    thing_cache = {}


    job.report("Preparing data")

    graphs = builder.fetch(METADATA_QUERY)

    for graph in graphs:
        builder.actors(ACTOR_QUERY.replace("__GRAPH__", graph['iri']['value']))
        builder.things(OBJECT_QUERY.replace("__GRAPH__", graph['iri']['value']))
        builder.thingCreators(OBJECT_CREATORS_QUERY.replace("__GRAPH__", graph['iri']['value']))
        builder.thingDates(OBJECT_DATE_QUERY.replace("__GRAPH__", graph['iri']['value']))

    builder.actorBirths(BIRTH_QUERY)
    builder.actorDeaths(DEATH_QUERY)
    builder.actorRoles(ROLE_QUERY)
    builder.actorAttributes(ATTRIBUTE_QUERY)
    builder.actorGroupConnections(GROUP_CONNECTIONS_QUERY)
    builder.actorPlaces(ACTOR_PLACE_QUERY)
    builder.transformActorPlaces()
    builder.transformActorDates()
    builder.transformThingDates()
    builder.addIncomingPersonCounts()
    builder.addIncomingThingCounts()
    builder.deduplicate()

    job.report("Building graph index...")

    for g in graphs:
        graph = corpus.get_content("NamedGraph")
        graph.iri = g['iri']['value']
        graph.name = g['name']['value']
        graph.short_name = g['short_name']['value']
        graph.save()
        graph_cache[g['iri']['value']] = graph.id

    job.report("Done.")

    job.report("Building places index...")

    for pkey in builder.placeData.keys():
        place = corpus.get_content("Place")
        place.iri = pkey
        place.name = builder.placeData[pkey]['label']
        if 'geo_coordinates' in builder.placeData[pkey]:
            place.coordinates = builder.placeData[pkey]['geo_coordinates']['coordinates']
            place.longitude = builder.placeData[pkey]['geo_coordinates']['coordinates'][0]
            place.latitude = builder.placeData[pkey]['geo_coordinates']['coordinates'][1]
            if len(place.coordinates) == 2:
                country = builder.getCountry(place.coordinates, country_shapes, corpus)
                if country:
                    place.country = country.id
        place.save()
        place_cache[pkey] = place.id

    job.report("Done.")

    job.report("Building groups index...")

    #iterate over each actor in the actor thing, and add it to the corpus.
    for pkey in builder.actorData.keys():
        
        if "Group" in builder.actorData[pkey]['classes']:
            group = corpus.get_content("Group")
            group.iri = pkey    

            if "place" in builder.actorData[pkey]:
                for p in builder.actorData[pkey]['place'].keys():
                    if p in place_cache:
                        group.places.append(place_cache[p])
                        place = corpus.get_content('Place', place_cache[p], only=['country'])
                        if place and place.country:
                            group.countries.append(place.country.id)

            if "incoming_count" in builder.actorData[pkey]:
                group.incoming_connections = builder.actorData[pkey]['incoming_count']

            for g in builder.actorData[pkey]['graphs']:
                if g in graph_cache:
                    group.graphs.append(graph_cache[g])

            for n in builder.actorData[pkey]['names']:
                group.names.append(n)

            for image in builder.actorData[pkey]['images']:
                group.images.append(image)


            ### removing duplicates

            if group.names:
                group.names = list(set(group.names))

            if group.images:
                group.images = list(set(group.images))

            if group.countries:
                group.countries = list(set(group.countries))

            if group.places:
                group.places = list(set(group.places))

            try:
                group.save()
                group_cache[pkey] = group.id
            except Exception as e:
                job.report("failed to save group {}: {}".format(pkey, e))

    job.report("Done.")

    job.report("Building persons index...")

    for pkey in builder.actorData.keys():
        if "Person" in builder.actorData[pkey]['classes']:
            person = corpus.get_content("Person")
            person.iri = pkey
            if 'birth_date' in builder.actorData[pkey]:
                # skipping BCE dates for now
                if not builder.actorData[pkey]['birth_date'].startswith("-"):
                    try:
                        person.birth_date = datetime.strptime(builder.actorData[pkey]['birth_date'], '%Y-%m-%d')
                    except Exception as e:
                        job.report("error: {}. iri: {}. offending date: {}".format(e, pkey, builder.actorData[pkey]['birth_date']))
            if 'death_date' in builder.actorData[pkey]:
                # skipping BCE dates for now
                if not builder.actorData[pkey]['death_date'].startswith("-"):
                    try:
                        person.death_date = datetime.strptime(builder.actorData[pkey]['death_date'], '%Y-%m-%d')
                    except Exception as e:
                        job.report("error: {}. iri: {}. offending date: {}".format(e, pkey, builder.actorData[pkey]['death_date']))

            if "birth_place" in builder.actorData[pkey]:
                for p in builder.actorData[pkey]['birth_place'].keys():
                    if p in place_cache:
                        person.birth_place = place_cache[p]
                        person.places.append(place_cache[p])
                        place = corpus.get_content('Place', place_cache[p], only=['country'])
                        if place and place.country:
                            person.countries.append(place.country.id)
                    
            if "death_place" in builder.actorData[pkey]:
                for p in builder.actorData[pkey]['death_place'].keys():
                    if p in place_cache:
                        person.death_place = place_cache[p]
                        person.places.append(place_cache[p])
                        place = corpus.get_content('Place', place_cache[p], only=['country'])
                        if place and place.country:
                            person.countries.append(place.country.id)

            if "place" in builder.actorData[pkey]:
                for p in builder.actorData[pkey]['place'].keys():
                    if p in place_cache:
                        person.places.append(place_cache[p])
                        place = corpus.get_content('Place', place_cache[p], only=['country'])
                        if place and place.country:
                            person.countries.append(place.country.id)

            if 'years_active' in builder.actorData[pkey]:
                person.years = builder.actorData[pkey]['years_active']

            if "incoming_count" in builder.actorData[pkey]:
                person.incoming_connections = builder.actorData[pkey]['incoming_count']

            for g in builder.actorData[pkey]['graphs']:
                if g in graph_cache:
                    person.graphs.append(graph_cache[g])

            for n in builder.actorData[pkey]['names']:
                person.names.append(n)

            for a in builder.actorData[pkey]['attributes']:
                person.attributes.append(a)
            
            for p in builder.actorData[pkey]['professions']:
                if p.lower() != 'occupation':
                    person.professions.append(p)

            for t in builder.actorData[pkey]['techniques']:
                person.techniques.append(t.lower())

            for n in builder.actorData[pkey]['notes']:
                person.notes.append(n)
            
            for image in builder.actorData[pkey]['images']:
                person.images.append(image)

            for t in builder.actorData[pkey]['techniques']:
                if t.lower().strip() in TYPE_LOOKUP:
                    if TYPE_LOOKUP[t.lower().strip()] not in person.broad_techniques:
                        person.broad_techniques.append(TYPE_LOOKUP[t.lower().strip()])
                else:
                    missing_types.append(t.lower().strip())
                    person.broad_techniques.append("other")

            ### removing duplicates

            if person.countries:
                person.countries = list(set(person.countries))

            if person.places:
                person.places = list(set(person.places))

            if person.professions:
                person.professions = list(set(person.professions))

            if person.images:
                person.images = list(set(person.images))

            if person.techniques:
                person.techniques = list(set(person.techniques))

            if person.broad_techniques:
                person.broad_techniques = list(set(person.broad_techniques))

            if person.years:
                person.years = list(set(person.years))
                person.decades = list(set([x - (x%10) for x in person.years]))
    
            try:
                person.save()
                person_cache[pkey] = person.id
            except Exception as e:
                job.report("failed to save person {}: {}".format(pkey, e))

        if len(missing_types)>0:
            job.report("The following types are missing from the lookup:")
            for t in list(set(missing_types)):
                job.report(t.lower().strip())

    job.report("Done.")

    job.report("reindexing with affiliated groups")
    
    for pkey in builder.actorData.keys():
        if "Person" in builder.actorData[pkey]['classes']:
            if 'affiliated_groups' in builder.actorData[pkey]:
                for group_iri in builder.actorData[pkey]['affiliated_groups']:
                    if group_iri in group_cache:
                        affiliation = corpus.get_content('PersonGroup')
                        group = corpus.get_content('Group', group_cache[group_iri])
                        for decade in person.decades:
                            if decade not in group.decades:
                                group.decades.append(decade)
                        affiliation.group_iri = group_iri
                        affiliation.person_iri = pkey
                        affiliation.person_name = builder.actorData[pkey]['names'][0]
                        affiliation.group_name = builder.actorData[group_iri]['names'][0]
                        try:
                            affiliation.save()
                        except Exception as e:
                            job.report("failed to save affiliation {}: {}".format(pkey, e))
                        try:
                            group.save()
                        except Exception as e:
                            job.report("failed to save group {}: {}".format(pkey, e))

    job.report("Done.")

    ## Building Thing Index

    job.report("Building things index...")

    counter = 0
    # iterate over the thingData dictionary
    for key in builder.thingData:
        if limit != counter:
            counter = counter + 1
            # create a new corpora instance for a LINCS thing
            thing = corpus.get_content("Thing")
            thing.iri = key
            thing.incoming_connections = builder.thingData[key]['incoming_count']
            
            for value in builder.thingData[key]['names']:
                thing.names.append(value)    
            
            for value in builder.thingData[key]['classes']:
                thing.classes.append(value)

            for value in builder.thingData[key]['notes']:
                thing.notes.append(value)

            for value in builder.thingData[key]['topics']:
                thing.topics.append(value)

            for value in builder.thingData[key]['images']:
                thing.images.append(value)

            for value in builder.thingData[key]['materials']:
                thing.materials.append(value)
                if value.lower().strip() in MATERIAL_LOOKUP:
                    if MATERIAL_LOOKUP[value.lower().strip()] not in thing.broad_materials:
                        thing.broad_materials.append(MATERIAL_LOOKUP[value.lower().strip()])
                else:
                    missing_materials.append(value.lower().strip())
                    thing.broad_materials.append("other")

            for value in builder.thingData[key]['types']:
                thing.types.append(value)                                    
                if value.lower().strip() in TYPE_LOOKUP:
                    if TYPE_LOOKUP[value.lower().strip()] not in thing.broad_types:
                        thing.broad_types.append(TYPE_LOOKUP[value.lower().strip()])
                else:
                    missing_types.append(value.lower().strip())
                    thing.broad_types.append("other")

            for date in builder.thingData[key]['date']:
                if len(date) == 4:
                    try:
                        thing.years.append(int(date))
                    except Exception as e:
                        job.report("error: {}. iri: {}. offending date: {}".format(e, key, date))

                elif len(date)>4:
                    try:
                        years = re.findall(r'\d+', date)
                        years = [datetime.strptime(date, '%Y') for date in re.findall(r'\d+', date)]
                        if len(years)>1:
                            for d in [datetime.strptime(str(y), '%Y') for y in range(min(years).year, max(years).year+1)]:
                                thing.years.append(int(str(d.year)))
                        else:
                            thing.years.append(years[0])
                    except Exception as e:
                        job.report("error: {}. iri: {}. offending year: {}".format(e, key, date))

            for p in builder.thingData[key]['places']:
                if p in place_cache:
                    thing.places.append(place_cache[p])
                    place = corpus.get_content('Place', place_cache[p], only=['country'])
                    if place and place.country:
                        thing.countries.append(place.country.id)
                else:
                    job.report("{} not in place cache".format(p))

            for g in builder.thingData[key]['graphs']:
                if g in graph_cache:
                    thing.graphs.append(graph_cache[g])
                else:
                    job.report("{} not in graph cache".format(g))

            if thing.countries:
                thing.countries = list(set(thing.countries))

            if thing.years:
                thing.years = list(set(thing.years))
                try:
                    thing.decades = list(set([int(x) - (int(x)%10) for x in thing.years]))
                except Exception as e:
                    job.report("{}. Date failed for {}: {}".format(e, key, thing.years))
            
            if thing.countries:
                thing.countries = list(set(thing.countries))

            if thing.places:
                thing.places = list(set(thing.places))

            if thing.classes:
                thing.classes = list(set(thing.classes))

            if thing.images:
                thing.images = list(set(thing.images))

            if thing.notes:
                thing.notes = list(set(thing.notes))

            if thing.materials:
                thing.materials = list(set(thing.materials))

            if thing.types:
                thing.types = list(set(thing.types))

            if thing.topics:
                thing.topics = list(set(thing.topics))

            try:
                thing.save()
                thing_cache[key] = thing.id
            except Exception as e:
                job.report("failed to save {}: {}".format(key, e))

            ## The "thing" has been saved above, but the instance is still in scope
            ## we want to reuse the thing id here.
            ## the current thing dictionary record is also still in scope
            ## we want to see if there are any owners (later, creators) of this thing
            ## we iterate over the owners in the thing
            ## we fetch the corpora record for the owner iri (person record)
            ## we add the thing as a "created" property value
            # then we save the person record and move on to the next thing

            for iri in builder.thingData[key]['owners']:        
                if iri in person_cache:
                    personThing = corpus.get_content('PersonThing')
                    personThing.person_iri = iri
                    personThing.thing_iri = key
                    personThing.person_name = builder.actorData[iri]['names'][0]
                    personThing.thing_name = builder.thingData[key]['names'][0]
                    personThing.relationship = "Owner"
                    try:
                        personThing.save()
                    except Exception as e:
                        job.report("failed to save {}: {}".format(key, e))

                if iri in group_cache:
                    groupThing = corpus.get_content('GroupThing')
                    groupThing.group_iri = iri
                    groupThing.thing_iri = key
                    groupThing.group_name = builder.actorData[iri]['names'][0]
                    groupThing.thing_name = builder.thingData[key]['names'][0]
                    groupThing.relationship = "Owner"
                    try:
                        groupThing.save()
                    except Exception as e:
                        job.report("failed to save {}: {}".format(key, e))

            for iri in builder.thingData[key]['creators']:
                if iri in person_cache:
                    personThing = corpus.get_content('PersonThing')
                    personThing.person_iri = iri
                    personThing.thing_iri = key
                    personThing.person_name = builder.actorData[iri]['names'][0]
                    personThing.thing_name = builder.thingData[key]['names'][0]
                    personThing.relationship = "Creator"
                    try:
                        personThing.save()
                    except Exception as e:
                        job.report("failed to save {}: {}".format(key, e))

                if iri in group_cache:
                    groupThing = corpus.get_content('GroupThing')
                    groupThing.group_iri = iri
                    groupThing.thing_iri = key
                    groupThing.group_name = builder.actorData[iri]['names'][0]
                    groupThing.thing_name = builder.thingData[key]['names'][0]
                    groupThing.relationship = "Creator"
                    try:
                        groupThing.save()
                    except Exception as e:
                        job.report("failed to save {}: {}".format(key, e))

    if len(missing_types)>0:
        job.report("The following types are missing from the lookup:")
        for t in list(set(missing_types)):
            job.report(t.lower().strip())

    if len(missing_materials)>0:
        job.report("The following materials are missing from the lookup:")
        for m in list(set(missing_materials)):
            job.report(m.lower().strip())

    job.report("Done.")